#include <stdio.h>

int main() {
  printf("- Bubblesort-Programm -\n");
  printf("Wie viele Eintraege soll die Liste haben?\n");
  printf("Geben Sie eine Zahl zwischen 1 und 10 ein und druecken Sie Enter.\n");
  int wert=0;
  scanf("%d", &wert);
  int liste[wert];
  int i;
  for(i = 1; i <= wert; i++) {
     printf("Bitte geben Sie die %d. Zahl ein (zwischen 1 und 10), dann Enter:\n", i);
     scanf("%d", &liste[i-1]);
  }
  printf("\n\nIhre eingegebene Liste lautet: ");
  for(i = 0; i < wert; i++) {
      if(i!=wert-1) printf("%d, ", liste[i]);
      else printf("%d\n", liste[i]);
  }
  printf("Die sortierte Liste lautet:    ");
  int buffer = 0;
  int z = 0;
  int count = 0;
  int u;
  int x;
  for(u = 0; u < wert; u++){
    for(x = 0; x < wert-1-z; x++){
            if(liste[x]>liste[x+1]) {
                buffer = liste[x];
                liste[x] = liste[x+1];
                liste[x+1] = buffer;
            }
            count ++;
        }
    z++;
    }
    for(x = 0; x < wert; x++){
        //cout << a[x] << ", "; 
        if(x!=wert-1) printf("%d, ", liste[x]);
        else printf("%d", liste[x]);
    }
  return 0;
}